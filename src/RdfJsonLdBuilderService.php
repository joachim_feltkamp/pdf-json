<?php

/**
 * @file
 * Contains \Drupal\rdfjsonld\RdfJsonLdBuilderService.
 */

namespace Drupal\rdfjsonld;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Entity\File;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\rdf\Entity\RdfMapping;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RdfJsonLdBuilderService.
 *
 * @package Drupal\rdfjsonld
 */
class RdfJsonLdBuilderService {

  /**
   * Symfony\Component\HttpFoundation\Request definition.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var RdfMapping
   */
  protected $mapping;

  /**
   * Constructor.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * @param EntityInterface $entity
   *
   * @return array
   */
  public function getMappedData(EntityInterface $entity) {
    $data = [];

    if ($this->getMapping($entity->getEntityTypeId() . '.' . $entity->bundle())) {
      $data["@context"] = "http://schema.org";
      // Set schema.org type.
      $data['@type'] = $this->getType();

      // Set schemo.org properties.
      foreach ($this->getMappedFields() as $key => $value) {
        $property_name = $this->getProperty($value);
        $property_value = $this->getPropertyValue($entity, $key, $property_name);

        if ($property_name && $property_value) {
          $data[$property_name] = $property_value;
        }
      }
    }

    return $data;
  }


  /**
   * Loads the RDF Mapping from RDF module.
   *
   * @param string $entity_id
   * @return RdfMapping|null
   */
  protected function getMapping($entity_id) {
    if (!$this->mapping || $this->mapping->id() !== $entity_id) {
      $this->mapping = RdfMapping::load($entity_id);
    }
    return ($this->mapping instanceof RdfMapping) ? $this->mapping : NULL;
  }



  /**
   * @param array $map
   * @param FieldItemList $field
   */
  protected function getProperty($value) {
    if (isset($value['properties'][0])) {
      return $this->cutSchema($value['properties'][0]);
    }
    return NULL;
  }

  /**
   * @return string
   */
  private function getType() {
    $type_array = $this->mapping->getPreparedBundleMapping();
    if (isset($type_array['types'][0])) {
      return $this->cutSchema($type_array['types'][0]);
    }
    return 'Article';
  }

  /**
   * @return array
   */
  private function getMappedFields() {
    $field_array = $this->mapping->toArray();
    if (isset($field_array['fieldMappings'])) {
      return $field_array['fieldMappings'];
    }
    return [];
  }

  /**
   * @param EntityInterface $entity
   * @param string $field
   * @return array|null|string
   */
  private function getPropertyValue(EntityInterface $entity, $field, $property_name) {
    if ($entity->hasField($field)) {
      /* @var FieldItemListInterface $field */
      $field = $entity->get($field);

      switch ($field->getFieldDefinition()->getType()) {
        case 'image':
          $value =  $this->getRdfImageObject($field);
          break;
        case 'created':
        case 'changed':
          $value = $this->getRdfDate($field);
          break;
        case 'entity_reference':
        case 'entity_reference_revisions':
          $value = $this->getRdfReferencedEntity($field, $property_name);
          break;
        default:
          $value = strip_tags($field->getString());

      }
      return $value;
    }
    return NULL;

  }

  /**
   * Transforms string 'schema:someValue' to 'someValue'.
   * @param string $string
   * @return string
   */
  private function cutSchema($string) {
    $array = explode(':', $string);
    if (reset($array) == 'schema') {
      unset($array[0]);
      $string = implode(':', $array);
    }
    return $string;
  }


  /**
   * @param FieldItemListInterface $field
   *
   * @return array|null
   */
  private function getRdfImageObject($field) {
    /* @var File $first_item */
    $image_object = reset($field->referencedEntities());
    $image_value = reset($field->getValue());
    if ($image_object && $image_value) {
      return [
        "@type" => "ImageObject",
        "url" => $image_object->url(),
        "width" => $image_value['width'],
        "height" => $image_value['height'],
      ];
    }
    return NULL;
  }

  /**
   * @param FieldItemListInterface $field
   *
   * @return string|null
   */
  private function getRdfDate($field) {
    /* @var File $first_item */
    $value = reset($field->getValue());
    if ($value) {
      return $value['_attributes']['content'];
    }
    return NULL;
  }


  /**
   * @param EntityReferenceFieldItemListInterface $field
   *
   * @return string|array|null
   */
  private function getRdfReferencedEntity($field, $property_name) {
    /* @var EntityReferenceFieldItemList $value */
    $value = $field->getValue();
    if (count($value)) {
      $entities = $field->referencedEntities();
      $text_collector = [];
      $data_collettor = [];
      foreach ($entities as $entity) {
        
        switch ($property_name) {
          case 'author':
              $data_collettor[] = [
                "@type" => "Person",
                "name" => $entity->label()
              ];
              break;
          case 'text':
            /* @var Paragraph $entity */
            $entity_view = entity_view($entity, 'rdf');
            $text_collector[] = strip_tags(\Drupal::service('renderer')->renderRoot($entity_view));
            break;
          default:
            $data_collettor[] = $this->getEntityShortRdf($entity);
        }
        
      }
      if (count($data_collettor) > 1) {
        return $data_collettor;
      } elseif (count($data_collettor)) {
        return $data_collettor[0];
      }
      else {
        return $this->implode("\n", $text_collector);
      }
    }
    return NULL;
  }

  /**
   * @param string $delimiter
   *   Delimiter.
   * @param array $array
   *   Value array.
   * @return mixed
   *   Value.
   */
  private function implode($delimiter, array $array) {
    $raw = implode($delimiter, $array);
    return preg_replace('/\\n[ \\n]+/', $delimiter, $raw);
  }


  /**
   * @param EntityInterface $entity
   *   The entity to map.
   *
   * @return array|null
   *   Mapped data as short reference.
   */
  public function getEntityShortRdf($entity) {
    $mapping = RdfMapping::load($entity->getEntityTypeId() . '.' . $entity->bundle());
    if ($mapping instanceof RdfMapping) {
      $type_array = $mapping->getPreparedBundleMapping();
      $type = (isset($type_array['types'][0]))
        ? $this->cutSchema($type_array['types'][0])
        : NULL;

      if ($type) {
        $data = ['@type' => $type];
        if ($name = $entity->label()) {
          $data['name'] = $name;
        }
        if ($url = $entity->toUrl()) {
          $data['url'] = $url;
        }
        return $data;
      }
    }
    return NULL;
  }

}
