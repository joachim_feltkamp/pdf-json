<?php

namespace Drupal\rdfjsonld\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'rdf_json_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "rdf_json_formatter_type",
 *   label = @Translation("Rdf json formatter type"),
 *   field_types = {
 *     "rdf_json_field_type"
 *   }
 * )
 */
class RdfJsonFormatterType extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
    /*    return [
      // Implement default settings.
      ] + parent::defaultSettings();
    */
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
    /*return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);*/
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $rdfjsonld_service = \Drupal::service('rdfjsonld.builder');

    $mapped = $rdfjsonld_service->getMappedData($items->getEntity());
    if ($mapped) {
      $jsonld = '<script type="application/ld+json">' . json_encode($mapped) . '</script>';

      $element['1'] = [
        '#markup' => $jsonld,
        '#weight' => 10,
        '#allowed_tags' => ['script'],
      ];
    }

    return $element;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
