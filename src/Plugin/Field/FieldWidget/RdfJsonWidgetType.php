<?php

namespace Drupal\rdfjsonld\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'rdf_json_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "rdf_json_widget_type",
 *   label = @Translation("Rdf json widget type"),
 *   field_types = {
 *     "rdf_json_field_type"
 *   }
 * )
 */
class RdfJsonWidgetType extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'enabled' => 1,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $elements['enabled'] = [
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('enabled'),
        '#title' => t('Default value.'),
      ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = 'default: ' . $this->getSetting('enabled');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'checkbox',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : $this->getSetting('enabled'),
      '#title' => t('Create schema.org of the content.'),
    ];

    return $element;
  }

}
